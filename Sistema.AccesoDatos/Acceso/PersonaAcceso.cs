﻿using Sistema.AccesoDatos.Clases;
using Sistema.AccesoDatos.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistema.AccesoDatos.Acceso
{
    public class PersonaAcceso
    {
        public IEnumerable<PersonaAccesoDB> ListarPersonas(string filtro)
        {
            IEnumerable<PersonaAccesoDB> lista = new List<PersonaAccesoDB>();
            using (SistemaBaseBDEntities ctx = new SistemaBaseBDEntities())
            {

                var listaDBLambda = ctx.tb_persona.Where(
                    x => x.primer_nombre.ToUpper().Contains(filtro.ToUpper()) ||
                    x.primer_nombre.ToUpper().Contains(filtro.ToUpper()) ||
                    x.primer_nombre.ToUpper().Contains(filtro.ToUpper()) ||
                    x.primer_nombre.ToUpper().Contains(filtro.ToUpper())
                    ).ToList();

                lista = new Mapeadores.PersonaAccesoMapeador().mapearT1T2(listaDBLambda);

            }
            return lista;
        }

    }
}
