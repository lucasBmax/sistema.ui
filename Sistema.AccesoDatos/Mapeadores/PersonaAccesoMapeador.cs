﻿using Sistema.AccesoDatos.Clases;
using Sistema.AccesoDatos.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistema.AccesoDatos.Mapeadores
{
    public class PersonaAccesoMapeador : MapeadorAccesoBase<tb_persona, PersonaAccesoDB>
    {
        public override PersonaAccesoDB mapearT1T2(tb_persona entrada)
        {
            return new PersonaAccesoDB()
            {
                Id = entrada.id,
                Primer_Nombre = entrada.primer_nombre,
                Segundo_Nombre = entrada.segundo_nombre
            };
        }

        public override IEnumerable<PersonaAccesoDB> mapearT1T2(IEnumerable<tb_persona> entrada)
        {
            foreach (var p in entrada)
            {
                yield return mapearT1T2(p);
            }
        }

        public override tb_persona mapearT2T1(PersonaAccesoDB entrada)
        {
            return new tb_persona()
            {
                id = entrada.Id,
                primer_nombre = entrada.Primer_Nombre,
                segundo_nombre = entrada.Segundo_Nombre
            };
        }

        public override IEnumerable<tb_persona> mapearT2T1(IEnumerable<PersonaAccesoDB> entrada)
        {
            foreach (var p in entrada)
            {
                yield return mapearT2T1(p);
            }
        }
    }
}
