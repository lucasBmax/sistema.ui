﻿using Sistema.LogicaNegocio.DTO;
using Sistema.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sistema.UI.Mapeadores
{
    public class MapeadorPersonaUI : MapeadorUIBase<PersonaNegocioDTO, PersonaUIModel>
    {
        public override PersonaUIModel mapearT1T2(PersonaNegocioDTO entrada)
        {
            return new PersonaUIModel()
            {
                Id = entrada.Id,
                Primer_Nombre = entrada.Primer_Nombre,
                Segundo_Nombre = entrada.Segundo_Nombre
            };
        }

        public override IEnumerable<PersonaUIModel> mapearT1T2(IEnumerable<PersonaNegocioDTO> entrada)
        {
            foreach (var p in entrada)
            {
                yield return mapearT1T2(p);
            }
        }

        public override PersonaNegocioDTO mapearT2T1(PersonaUIModel entrada)
        {
            return new PersonaNegocioDTO()
            {
                Id = entrada.Id,
                Primer_Nombre = entrada.Primer_Nombre,
                Segundo_Nombre = entrada.Segundo_Nombre
            };
        }

        public override IEnumerable<PersonaNegocioDTO> mapearT2T1(IEnumerable<PersonaUIModel> entrada)
        {
            foreach (var p in entrada)
            {
                yield return mapearT2T1(p);
            }
        }
    }
}