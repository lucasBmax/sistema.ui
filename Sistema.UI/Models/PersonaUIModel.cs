﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sistema.UI.Models
{
    public class PersonaUIModel
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }


        private string primer_nombre;

        public string Primer_Nombre
        {
            get { return primer_nombre; }
            set { primer_nombre = value; }
        }


        private string segundo_nombre;

        public string Segundo_Nombre
        {
            get { return segundo_nombre; }
            set { segundo_nombre = value; }
        }
    }

    public class ListaPersonaUIModel
    {
        private IEnumerable<PersonaUIModel> listaPersonas;

        public IEnumerable<PersonaUIModel> ListaPersonas
        {
            get { return listaPersonas; }
            set { listaPersonas = value; }
        }

    }

}