﻿using Sistema.LogicaNegocio.Negocio;
using Sistema.UI.Mapeadores;
using Sistema.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sistema.UI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string filtro = "")
        {
            PersonaNegocio negocio = new PersonaNegocio();
            MapeadorPersonaUI mapeador = new MapeadorPersonaUI();
            ListaPersonaUIModel modelo = new ListaPersonaUIModel()
            {
                ListaPersonas = mapeador.mapearT1T2(negocio.ListarPersonas(filtro))
            };
            return View(modelo);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}