﻿using Sistema.AccesoDatos.Acceso;
using Sistema.AccesoDatos.Clases;
using Sistema.LogicaNegocio.DTO;
using Sistema.LogicaNegocio.Mapeadores;
using System.Collections.Generic;

namespace Sistema.LogicaNegocio.Negocio
{
    public class PersonaNegocio
    {
        private PersonaAcceso acceso;

        public PersonaNegocio()
        {
            this.acceso = new PersonaAcceso();
        }

        public IEnumerable<PersonaNegocioDTO> ListarPersonas(string filtro)
        {
            MapeadorPersonaNegocio mapeador = new MapeadorPersonaNegocio();
            IEnumerable<PersonaNegocioDTO> lista =  mapeador.mapearT1T2(this.acceso.ListarPersonas(filtro));
            return lista;
        }


    }
}
