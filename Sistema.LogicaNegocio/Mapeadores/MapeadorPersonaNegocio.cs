﻿using Sistema.AccesoDatos.Clases;
using Sistema.LogicaNegocio.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistema.LogicaNegocio.Mapeadores
{
    public class MapeadorPersonaNegocio : MapeadorNegocioBase<PersonaAccesoDB, PersonaNegocioDTO>
    {
        public override PersonaNegocioDTO mapearT1T2(PersonaAccesoDB entrada)
        {
            return new PersonaNegocioDTO()
            {
                Id = entrada.Id,
                Primer_Nombre = entrada.Primer_Nombre,
                Segundo_Nombre = entrada.Segundo_Nombre
            };
        }

        public override IEnumerable<PersonaNegocioDTO> mapearT1T2(IEnumerable<PersonaAccesoDB> entrada)
        {
            foreach (var p in entrada)
            {
                yield return mapearT1T2(p);
            }
        }

        public override PersonaAccesoDB mapearT2T1(PersonaNegocioDTO entrada)
        {
            return new PersonaAccesoDB()
            {
                Id = entrada.Id,
                Primer_Nombre = entrada.Primer_Nombre,
                Segundo_Nombre = entrada.Segundo_Nombre
            };
        }

        public override IEnumerable<PersonaAccesoDB> mapearT2T1(IEnumerable<PersonaNegocioDTO> entrada)
        {
            foreach (var p in entrada)
            {
                yield return mapearT2T1(p);
            }
        }
    }
}
