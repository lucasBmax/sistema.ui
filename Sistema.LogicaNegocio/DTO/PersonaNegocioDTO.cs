﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sistema.LogicaNegocio.DTO
{
    public class PersonaNegocioDTO
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }


        private string primer_nombre;

        public string Primer_Nombre
        {
            get { return primer_nombre; }
            set { primer_nombre = value; }
        }


        private string segundo_nombre;

        public string Segundo_Nombre
        {
            get { return segundo_nombre; }
            set { segundo_nombre = value; }
        }
    }
}
